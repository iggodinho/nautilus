# Code Guide #

**Code instructions:** Type python3 fibonacci.py or sum.py in the terminal to use the code.

**multiples_sum.py:** Using a for loop, the program stores in a list the multiples of 3 or 5 below 1000 and prints their sum at the end.

**fibonacci.py:** The program returns the sum of the even numbers in a fibonacci sequence whose values ​​do not exceed four million.



