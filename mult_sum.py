#https://projecteuler.net/problem=1

L = [] #empty list
for i in range(1000): #the last number is 999
    if i%3 == 0 or i%5 == 0: #checking if the number is a multiple of 3 or 5
      L.append(i) #number is added to the list 
answer=sum(L) #function to sum all the numbers in the list      
print(answer) 
